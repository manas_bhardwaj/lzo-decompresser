package factory;

import com.google.protobuf.Message;
import com.hadoop.compression.lzo.LzopCodec;
import com.twitter.elephantbird.mapreduce.io.ProtobufBlockReader;
import com.twitter.elephantbird.util.TypeRef;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.compress.CompressionInputStream;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class LzoProtoReaderWriterFactory {

    public static <M extends Message> ProtobufBlockReader<M> getProtobufBlockReader(TypeRef<M> typeRef, File file) throws IOException {
        LzopCodec codec = new LzopCodec();
        codec.setConf(new Configuration());
        CompressionInputStream inputStream = codec.createInputStream(new FileInputStream(file));
        ProtobufBlockReader<M> protobufBlockReader = new ProtobufBlockReader<>(inputStream, typeRef);
        return protobufBlockReader;
    }
}
