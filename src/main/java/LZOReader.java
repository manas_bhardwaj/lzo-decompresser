import com.cleartrip.analytics.*;
import com.cleartrip.analytics.UiEvents.UiEvent;
import factory.LzoProtoReaderWriterFactory;
import com.google.protobuf.Message;
import com.twitter.elephantbird.mapreduce.io.ProtobufBlockReader;
import com.twitter.elephantbird.util.TypeRef;

import java.io.*;
import java.util.*;

public class LZOReader {

    private String basePath;
    private static final String ERRED = "erred";

    public static String PATH_SEPARATOR = "/";
    private static String IN_PROCESS = "in-process";
    private static String IN_PROGRESS = "in-progress";
    private static String QUEUED = "queued";
    String merged = "merged";


    public LZOReader(String basePath) {
        this.basePath = basePath;
    }

    private ArrayList<File> files(String basePath, ArrayList<File> fileList) {
        File baseFilePath = new File(basePath);
        File[] files = baseFilePath.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    files(file.getAbsolutePath(), fileList);
                } else if (file.getAbsolutePath().endsWith(".lzo") && !ignoreDirectory(files))
                    fileList.add(file);
            }
        } else {
            fileList.add(baseFilePath);
        }
        return fileList;
    }

    private boolean ignoreDirectory(File[] files) {
        List<File> asList = Arrays.asList(files);
        boolean ignoreDir = false;
        for (File file : asList) {
            String fileName = file.getName();
            if (fileName.endsWith(ERRED) || fileName.endsWith(IN_PROCESS) || fileName.endsWith(IN_PROGRESS) || fileName.endsWith(QUEUED))
                ignoreDir = true;
        }
        return ignoreDir;
    }

    public <M extends Message> int readMessages(String product) throws IOException {
        return readMessages(product, basePath);
    }

    public <M extends Message> int readMessages(String product, String path) throws IOException {
        int totalRead = 0;
        TypeRef<M> typeRef = instance(product);
        System.out.println("Type ref returned based on the product is " + typeRef.getRawClass().getSimpleName());
        ArrayList<File> fileList = new ArrayList<>();
        ArrayList<File> files = files(path, fileList);
        for (File file : files) {
            System.out.println("File being read " + file.getAbsolutePath());
            ProtobufBlockReader<M> protobufBlockReader = LzoProtoReaderWriterFactory.getProtobufBlockReader(typeRef, file);
            M t;
            while ((t = protobufBlockReader.readNext()) != null) {
                switch (typeRef.getRawClass().getSimpleName()) {
                    case "AirMessage": {
                        Air.AirMessage mesage = (Air.AirMessage) t;
//                        System.out.println(mesage.getMessageType());
//                        System.out.println(mesage.getHeader().getSearchId());
                        if(mesage.getHeader().getSearchId().toString().equals("mo-v2-7d38bd9d-3de4-44e5-bee9-7beef374e6f5")){
                            System.out.println("FOUND!!!! "+ mesage.getHeader().getSearchId());
                            break;
                        };
                        break;
                    }
                    case "HotelMessage": {
                        Hotels.HotelMessage mesage = (Hotels.HotelMessage) t;
                        BufferedWriter out = new BufferedWriter(new FileWriter("/home/manasbhardwaj/Downloads/merger_hotel_20200908/output.txt", true));
                        out.write(String.valueOf(mesage));
                        out.close();
//                        System.out.println(mesage);
                        break;
                    }
                    default: {
                        Air.AirMessage mesage = (Air.AirMessage) t;
//                        System.out.println(mesage.getMessageType());
                        break;
                    }
                }
                totalRead++;
            }
        }
        System.out.println("Total read proto messages count is : " + totalRead);
        return totalRead;
    }

    FileOutputStream fos;

    private void prepareTowrite() throws FileNotFoundException {
        fos = new FileOutputStream("../lzo-sample-data/message.out");
    }

    private void persistMessage(Air.AirMessage airMessage) throws IOException {
        airMessage.writeDelimitedTo(fos);
    }

    private void prepareToClose() throws IOException {
        fos.flush();
        fos.close();
    }

    public static TypeRef instance(String product) {
        product = product.toLowerCase();
        switch (product) {
            case "air": {
                return new TypeRef<Air.AirMessage>() {
                };
            }
            case "hotel": {
                return new TypeRef<Hotels.HotelMessage>() {
                };
            }
            case "local": {
                return new TypeRef<Local.LocalMessage>() {
                };
            }

            case "marketing": {
                return new TypeRef<Marketing.CampaignData>() {
                };
            }

            case "payment": {
                return new TypeRef<Payments.PaymentMessage>() {
                };
            }
            case "ui": {
                return new TypeRef<UiEvent>() {
                };
            }
            default: {
                // by default we assume it to mergerProperty, threadSafeQueue,
                return new TypeRef<Air.AirMessage>() {
                };
            }
        }
    }

    private Set<String> listDirectories() {
        ArrayList<File> fileList = new ArrayList<>();
        fileList = files(basePath, fileList);
        Set<String> directories = new HashSet<>(fileList.size());
        for (File file : fileList) {
            directories.add(file.getParent());
        }
        return directories;
    }


    private void recursivelyReadAndCompare(String product, LZOReader lzoReader) throws IOException {
        Set<String> directories = lzoReader.listDirectories();
        System.out.println(directories.toString());
        for (String directory : directories) {
            System.out.println(" ----------------------------------------------------------------------------------------------------- ");
            System.out.println(" Reading messages from directory " + directory );
            int iOriginal = lzoReader.readMessages(product, directory);
//            directory = lzoReader.appendMerged(directory);
        }
    }

    public static void main(String[] args) throws IOException {
        String product = "hotel";
        String basePath = "/home/manasbhardwaj/Downloads/merger_hotel_20200908";
        if (args.length > 0 && !args[0].isEmpty())
            basePath = args[0];
        if (args.length > 1 && !args[1].isEmpty())
            product = args[1];

        System.out.println("Base path is " + basePath);
        System.out.println("product is " + product);
        LZOReader lzoReader = new LZOReader(basePath);
        lzoReader.recursivelyReadAndCompare(product, lzoReader);
    }
}